#pragma once

#include "blog.h"
#include "argo_string.h"

#include <wchar.h>

typedef struct ArgoEntryRaw *ArgoEntry;

extern ArgoEntry ArgoEntry_createEmpty();
extern ArgoEntry ArgoEntry_createFromFile();
extern void ArgoEntry_delete(ArgoEntry* self);

extern ArgoStr ArgoEntry_getTitle(ArgoEntry self);
extern ArgoStr ArgoEntry_getContent(ArgoEntry self);

extern void ArgoEntry_setTitle(ArgoEntry self, ArgoStr title);
extern void ArgoEntry_setContent(ArgoEntry self, ArgoStr content);

extern void ArgoEntry_saveToFile(ArgoEntry self, char* path);

extern void ArgoEntry_save(ArgoEntry* self);
#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct ArgoStrRaw *ArgoStr;

/**
 * Creates an empty string. Is equal to create("")
 */
extern ArgoStr ArgoStr_createEmpty();

/**
 * Creates an empty string that allocates enough memory
 * to hold the given amount of characters
 */
extern ArgoStr ArgoStr_createReserveSize(size_t size);

/**
 * Creates an string from the given cstring.
 */
extern ArgoStr ArgoStr_create(char* str);


extern ArgoStr ArgoStr_createFromFile(char* path);

/**
 * Free the string and it's resources.
 */
extern void ArgoStr_delete(ArgoStr* self);

extern ArgoStr ArgoStr_copy(ArgoStr self);

/**
 *
 */
extern void ArgoStr_reserveSpace(ArgoStr self, size_t wanted_size,
        bool nextPowerOfTwo);

/**
 * Reduces the allocated memory of this string to the bare minimum.
 * This function does not change the content of the string.
 */
extern void ArgoStr_clean(ArgoStr self);

extern size_t ArgoStr_size(ArgoStr self);

/**
 * Returns the character at the given position or the null-terminator
 * in case the index was invalid
 */
extern char ArgoStr_getChecked(ArgoStr self, size_t index);

/**
 * Returns the character at the given position. This function is undefined
 * for an invalid index.
 */
extern char ArgoStr_get(ArgoStr self, size_t index);

extern void ArgoStr_set(ArgoStr self, size_t index, char c);

/**
 * Appends a character at the end of this string.
 */
extern void ArgoStr_addChar(ArgoStr self, char c);

/**
 * Finds the given token in this string.
 * @param item the token to search for
 * @param pos
 * @returns true if and only if the token was found.
 *
 */
extern bool ArgoStr_find(ArgoStr self, char* item, int* pos);

extern ArgoStr ArgoStr_trim(ArgoStr self);

/**
 * Returns true if the given string contains the specified token.
 */
extern bool ArgoStr_contains(ArgoStr self, char* item);


extern ArgoStr ArgoStr_substring(ArgoStr self, size_t start, size_t end);

/**
 * Returns true if the given string is identical with this string
 */
extern bool ArgoStr_equal(ArgoStr self, ArgoStr other);

extern int ArgoStr_toInt(ArgoStr self, bool* error);

/**
 *
 */
extern void ArgoStr_writeToFile(ArgoStr self, FILE* file);

#include "entry.h"

#include <malloc.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>

#include "sys/types.h"
#include "sys/stat.h"


typedef struct ArgoEntryRaw{
	ArgoStr title;
	ArgoStr content;
	ArgoBlog blog;
} *ArgoEntry, ArgoEntryRaw;

ArgoEntry ArgoEntry_createEmpty() {
	ArgoEntry self = malloc(sizeof(ArgoEntryRaw));
	self->title = ArgoStr_create("");
	self->content = ArgoStr_create("");
	return self;
}

ArgoEntry ArgoEntry_createFromFile(char* path) {
	ArgoEntry self = ArgoEntry_createEmpty();

	bool readingTitle = true;
	FILE* file = fopen(path, "r");

    	if(file) {
		char c;
		while((c = fgetc(file)) != EOF) {
			if(readingTitle) {
				// We hit the char that separates title from content
				if(c == 23) {
					readingTitle = false;

					// We get the size of the file
					struct stat st;
					stat(path, &st);
					off_t bytesLeft = st.st_size - ArgoStr_size(self->title);
					if(bytesLeft > 0) {
						ArgoStr_reserveSpace(self->content, (size_t) bytesLeft, false);
					} else {
						break;
					}
				} else {
					ArgoStr_addChar(self->title, c);
				}
			} else {
				ArgoStr_addChar(self->content, c);
			}
		}
		fclose(file);
	} else {
		assert(false);
	}

	return self;
}

void ArgoEntry_delete(ArgoEntry* self) {
	ArgoStr_delete(&(*self)->title);
	ArgoStr_delete(&(*self)->content);
	free(*self);
}

ArgoStr ArgoEntry_getTitle(ArgoEntry self) {
	return self->title;
}

ArgoStr ArgoEntry_getContent(ArgoEntry self) {
	return self->content;
}

void ArgoEntry_setTitle(ArgoEntry self, ArgoStr title) {
	ArgoStr_delete(&self->title);
	self->title = ArgoStr_copy(title);
}

void ArgoEntry_setContent(ArgoEntry self, ArgoStr content) {
	ArgoStr_delete(&self->content);
	self->content = ArgoStr_copy(content);
}

void ArgoEntry_saveToFile(ArgoEntry self, char* path) {
	FILE* file = fopen(path, "w");
	if(file) {
		ArgoStr_writeToFile(self->title, file);
		fputc(23, file);
		ArgoStr_writeToFile(self->content, file);
	} else {
		assert(false);
	}
	fflush(file);
	fclose(file);
}
#include "argo_string.h"

#include <string.h>
#include <stdio.h>


struct ArgoStrRaw {
    char* content;
    size_t size;
    size_t preservedSize;
} ArgoStrRaw;

ArgoStr ArgoStr_createEmpty() {
    return ArgoStr_createReserveSize(0);
}

ArgoStr ArgoStr_createReserveSize(size_t size) {
    ArgoStr self = malloc(sizeof(ArgoStrRaw));
    self->content = malloc(sizeof(char) * size);
    self->size = 0;
    self->preservedSize = size;
    return self;
}

ArgoStr ArgoStr_create(char* str) {
    ArgoStr self = ArgoStr_createReserveSize(strlen(str));
    size_t len = strlen(str);
    memcpy(self->content, str, sizeof(char) * len);
    self->size = len;
    return self;
}

void ArgoStr_delete(ArgoStr* self) {
    if((*self)->content != NULL)
        free((*self)->content);
    free(*self);
    self = NULL;
}

ArgoStr ArgoStr_copy(ArgoStr self) {
	ArgoStr result = malloc(sizeof(ArgoStrRaw));

	size_t contentBytes = sizeof(char) * self->size;
	result->content = malloc(contentBytes);
	memcpy(result->content, self->content, contentBytes);

	result->preservedSize = result->size = self->size;

	return result;
}

size_t ArgoStr_size(ArgoStr self) {
    return self->size;
}

bool ArgoStr_find(ArgoStr self, char* item, int* pos) {
    size_t len = strlen(item);
    size_t runIndex, i = 0;

    if(len == 0 || self->size == 0)
        return false;
    if(pos != NULL) {
        if((*pos) >= self->size) {
            return false;
        }
        i = *pos;
    }
    for(; i <= self->size - len; i++) {
        for(runIndex = i; runIndex < i + len; runIndex++) {
            if(item[runIndex - i] != ArgoStr_get(self, runIndex)) {
                break;
            } else if (runIndex == i + len - 1) {
                if(pos != NULL)
                    (*pos) = i;
                return true;
            }
        }
    }
    return false;
}

bool ArgoStr_contains(ArgoStr self, char* item) {
    return ArgoStr_find(self, item, NULL);
}

void ArgoStr_reserveSpace(ArgoStr self, size_t wantedSize,
        bool nextPowerOfTwo) {

    if(wantedSize > self->preservedSize) {
        if(nextPowerOfTwo) {
		// In case we have a size of zero the normal
		// left-bitshift tactic doesn't work, so we
		// have to handle this case manually
		if(self->preservedSize == 0) {
			self->preservedSize++;
		}
		do {
			self->preservedSize <<= 1;
		} while(wantedSize > self->preservedSize);

        } else {
            self->preservedSize = wantedSize;
        }
        self->content = realloc(self->content,
             sizeof(char) * self->preservedSize);
    }
}

char ArgoStr_getChecked(ArgoStr self, size_t index) {
    if(index < self->size)
        return self->content[index];
    return '\0';
}

char ArgoStr_get(ArgoStr self, size_t index) {
    return self->content[index];
}

void ArgoStr_addChar(ArgoStr self, char c) {
    ArgoStr_reserveSpace(self, self->size + 1, true);
    self->content[self->size] = c;
    self->size++;
}

bool ArgoStr_equal(ArgoStr self, ArgoStr other) {
	if(self->size != other->size)
		return false;

	for(size_t i = 0; i < other->size; i++) {
		if(ArgoStr_get(self, i) != ArgoStr_get(self, i))
			return false;
	}
	return true;
}

int ArgoStr_toInt(ArgoStr self, bool* error) {
    int result = 0;
    size_t i = 0;
    bool negative = false;

    if(self->size == 0) {
        if(error != NULL)
            *error = true;
        return true;
    }

    negative = (ArgoStr_get(self, 0) == '-');

    if(negative)
        i = 1;

    for(; i < self->size; i++) {
        result *= 10;
        char c = ArgoStr_get(self, i);
        if(c < '0' || c > '9') {
            if(error != NULL)
                *error = true;
            return 0;
        }
        result += (c - '0');
    }

    return negative ? -result : result;
}

ArgoStr ArgoStr_trim(ArgoStr self) {
    size_t startPos;
    size_t endPos;

    if(self->size == 0)
        return ArgoStr_createEmpty();

    for(size_t i = 0; i < self->size; i++) {
        if(ArgoStr_get(self, i) != ' ') {
            startPos = i;
            break;
        }
        // only spaces in the string => return a empty-string
        if(i == self->size - 1) {
            return ArgoStr_createEmpty();
        }
    }

    for(size_t i = self->size - 1; i >= 0; i--) {
        if(ArgoStr_get(self, i) != ' ') {
            endPos = i + 1;
            break;
        }
    }
    return ArgoStr_substring(self, startPos, endPos);
}

void ArgoStr_set(ArgoStr self, size_t index, char c) {
    self->content[index] = c;
}

ArgoStr ArgoStr_substring(ArgoStr self, size_t start, size_t end) {
    if(end < start || start >= self->size || end > self->size)
        return NULL;

    if(end == start)
        return ArgoStr_createEmpty();

    ArgoStr result = ArgoStr_createReserveSize(end - start);
    result->size = end - start;
    for(size_t i = start; i < end; i++) {
        ArgoStr_set(result, i - start, ArgoStr_get(self, i));
    }

    return result;
}

void ArgoStr_clean(ArgoStr self) {
    self->content = realloc(self->content,
	sizeof(char) * self->size);
    self->preservedSize = self->size;
}

void ArgoStr_writeToFile(ArgoStr self, FILE* file) {
    for(size_t i = 0; i < self->size; i++) {
        fputc(ArgoStr_get(self, i), file);
    }
}
#include "argo_string.h"
#include <assert.h>

int main() {
    ArgoStr string = ArgoStr_createEmpty();
    ArgoStr_clean(string);
    assert(ArgoStr_getChecked(string, 0) == '\0');
    assert(ArgoStr_getChecked(string, 1) == '\0');
    assert(ArgoStr_getChecked(string, 2) == '\0');

    ArgoStr_addChar(string, 'a');

    assert(ArgoStr_getChecked(string, 0) == 'a');
    assert(ArgoStr_getChecked(string, 1) == '\0');

    ArgoStr_delete(&string);

    string = ArgoStr_create("");
    ArgoStr_clean(string);
    assert(ArgoStr_getChecked(string, 0) == '\0');
    assert(ArgoStr_getChecked(string, 1) == '\0');
    assert(ArgoStr_getChecked(string, 2) == '\0');

    ArgoStr_addChar(string, 'a');

    assert(ArgoStr_getChecked(string, 0) == 'a');
    assert(ArgoStr_getChecked(string, 1) == '\0');

    ArgoStr_delete(&string);
}
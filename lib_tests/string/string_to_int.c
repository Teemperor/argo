#include "argo_string.h"
#include <assert.h>

void test(char* str, int wantedResult, bool isGood) {
    ArgoStr string = ArgoStr_create(str);
    bool error;
    int result = ArgoStr_toInt(string, NULL);

    if(isGood)
        assert(result == wantedResult);

    result = ArgoStr_toInt(string, &error);

    if(isGood)
        assert(result == wantedResult);
    assert(error != isGood);

    ArgoStr_delete(&string);
}

int main() {
    test("-1000", -1000, true);
    test("1000", 1000, true);
    test("01000", 1000, true);
    test("0", 0, true);
    test("-1", -1, true);
    test("-99", -99, true);
    test("-99", -99, true);
    test("-01", -1, true);

    test("aasdf", 0, false);
    test("", 0, false);
    test("-", 0, false);
    test("-d10", 0, false);
    test("-0234d", 0, false);
    test("d1000", 0, false);
}
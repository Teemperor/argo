#include "argo_string.h"
#include <assert.h>

int main() {
    ArgoStr string = ArgoStr_create("tes");
    ArgoStr_clean(string);
    assert(ArgoStr_getChecked(string, 0) == 't');
    assert(ArgoStr_getChecked(string, 1) == 'e');
    assert(ArgoStr_getChecked(string, 2) == 's');
    assert(ArgoStr_getChecked(string, 3) == '\0');
    ArgoStr_delete(&string);
}
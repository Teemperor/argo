#include "argo_string.h"
#include <assert.h>

int main() {
    ArgoStr string = ArgoStr_create("tesg");
    assert(ArgoStr_contains(string, "t"));
    assert(ArgoStr_contains(string, "e"));
    assert(ArgoStr_contains(string, "g"));
    assert(ArgoStr_contains(string, "sg"));
    assert(ArgoStr_contains(string, "tesg"));

    assert(!ArgoStr_contains(string, "ga"));
    assert(!ArgoStr_contains(string, "ts"));

    ArgoStr_delete(&string);

    string = ArgoStr_create("");

    assert(!ArgoStr_contains(string, "ga"));
    assert(!ArgoStr_contains(string, "ts"));

    ArgoStr_delete(&string);
}
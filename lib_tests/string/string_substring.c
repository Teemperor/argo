#include "argo_string.h"
#include <assert.h>

int main() {
    ArgoStr string = ArgoStr_create("tesl");

    ArgoStr result1 = ArgoStr_create("t");
    ArgoStr result2 = ArgoStr_create("es");
    ArgoStr result3 = ArgoStr_create("l");

    ArgoStr substring1 = ArgoStr_substring(string, 0, 1);
    ArgoStr substring2 = ArgoStr_substring(string, 1, 3);
    ArgoStr substring3 = ArgoStr_substring(string, 3, 4);

    assert(ArgoStr_equal(result1, substring1));
    assert(ArgoStr_equal(result2, substring2));
    assert(ArgoStr_equal(result3, substring3));

    assert(ArgoStr_substring(string, 0, 5) == NULL);
    assert(ArgoStr_substring(string, 5, 6) == NULL);
    assert(ArgoStr_substring(string, 8, 7) == NULL);
    assert(ArgoStr_substring(string, 4, 3) == NULL);

    ArgoStr_delete(&string);
    ArgoStr_delete(&result1);
    ArgoStr_delete(&result2);
    ArgoStr_delete(&result3);
    ArgoStr_delete(&substring1);
    ArgoStr_delete(&substring2);
    ArgoStr_delete(&substring3);
}
#include "argo_string.h"
#include <assert.h>

int main() {
    ArgoStr string1 = ArgoStr_create(" tesl ");
    ArgoStr string2 = ArgoStr_create("tesl");
    ArgoStr string3 = ArgoStr_create(" tesl");
    ArgoStr string4 = ArgoStr_create("tesl ");
    ArgoStr string5 = ArgoStr_create(" ");
    ArgoStr string6 = ArgoStr_create("");

    ArgoStr result1234 = ArgoStr_create("tesl");
    ArgoStr result56 = ArgoStr_create("");

    ArgoStr test1 = ArgoStr_trim(string1);
    ArgoStr test2 = ArgoStr_trim(string2);
    ArgoStr test3 = ArgoStr_trim(string3);
    ArgoStr test4 = ArgoStr_trim(string4);
    ArgoStr test5 = ArgoStr_trim(string5);
    ArgoStr test6 = ArgoStr_trim(string6);

    assert(ArgoStr_equal(result1234, test1));
    assert(ArgoStr_equal(result1234, test2));
    assert(ArgoStr_equal(result1234, test3));
    assert(ArgoStr_equal(result1234, test4));
    assert(ArgoStr_equal(result56, test5));
    assert(ArgoStr_equal(result56, test6));

    ArgoStr_delete(&test1);
    ArgoStr_delete(&test2);
    ArgoStr_delete(&test3);
    ArgoStr_delete(&test4);
    ArgoStr_delete(&test5);
    ArgoStr_delete(&test6);


    ArgoStr_delete(&result1234);
    ArgoStr_delete(&result56);

    ArgoStr_delete(&string1);
    ArgoStr_delete(&string2);
    ArgoStr_delete(&string3);
    ArgoStr_delete(&string4);
    ArgoStr_delete(&string5);
    ArgoStr_delete(&string6);
}
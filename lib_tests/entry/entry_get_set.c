#include "entry.h"
#include <assert.h>

int main() {
	ArgoEntry entry = ArgoEntry_createEmpty();

	ArgoStr title = ArgoStr_create("");
	ArgoStr content = ArgoStr_create("");

	ArgoEntry_setTitle(entry, title);
	ArgoEntry_setContent(entry, content);

	assert(ArgoStr_equal(ArgoEntry_getTitle(entry), title));
	assert(ArgoStr_equal(ArgoEntry_getContent(entry), content));

	ArgoStr_delete(&title);
	ArgoEntry_delete(&entry);
}
#include "entry.h"
#include <assert.h>
#include <errno.h>

int main() {
	ArgoEntry entry = ArgoEntry_createEmpty();

	ArgoStr title = ArgoStr_create("title");
	ArgoStr content = ArgoStr_create("content");

	ArgoEntry_setTitle(entry, title);
	ArgoEntry_setContent(entry, content);

	ArgoEntry_saveToFile(entry, "test.entry");
	
	ArgoEntry_delete(&entry);

	entry = ArgoEntry_createFromFile("test.entry");

	assert(ArgoStr_equal(ArgoEntry_getTitle(entry), title));
	assert(ArgoStr_equal(ArgoEntry_getContent(entry), content));

	ArgoStr_delete(&title);
	ArgoStr_delete(&content);

	ArgoEntry_delete(&entry);
}